#! /bin/bash

#########################################################################
#
#   脚本功能：安装编译环境
#   编辑时间：2023.06.30
#
#########################################################################

red="\e[0;31m"    # 红色
RED="\e[1;31m"
green="\e[0;32m"  # 绿色
GREEN="\e[1;32m"
yellow="\e[0;33m" # 黄色
YELLOW="\e[1;33m"
blue="\e[0;34m"   # 蓝色
BLUE="\e[1;34m"
purple="\e[0;35m" # 紫色
PURPLE="\e[1;35m"
cyan="\e[0;36m"   # 蓝绿色
CYAN="\e[1;36m"
WHITE="\e[1;37m"  # 白色
NC="\e[0m"        # 没有颜色

echo -e "${yellow}>>> 安装依赖包${NC}"
sudo apt update
sudo apt install -y curl snap

echo -e "${yellow}>>> 安装 Java、Kotlin${NC}"
sudo apt install -y default-jdk
sudo snap install kotlin --classic

echo -e "${yellow}>>> 安装 Mono${NC}"
sudo apt install -y mono-devel mono-vbnc

echo -e "${yellow}>>> 安装 PHP${NC}"
sudo apt install -y php

echo -e "${yellow}>>> 安装 Ruby${NC}"
sudo apt install -y ruby-full

echo -e "${yellow}>>> 安装 Go${NC}"
sudo apt install -y golang

echo -e "${yellow}>>> 安装 Rust${NC}"
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
source $HOME/.cargo/env

echo -e "${yellow}>>> 安装 NodeJS、TypeScript${NC}"
curl -sL https://deb.nodesource.com/setup_20.x | sudo -E bash -
sudo apt update
sudo apt install -y nodejs
sudo npm install -g typescript

echo -e "${yellow}>>> 安装 Lua${NC}"
sudo apt install -y lua5.3
