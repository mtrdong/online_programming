# online_programming

#### 介绍
多语言在线编程Demo


#### 安装教程

1.  安装 [Python](https://mirrors.huaweicloud.com/python/) 环境
2.  安装依赖包
```shell
pip install -r requirements.txt
```
3.  安装配置多语言编译、运行环境
```shell
# 赋予脚本执行权限并执行脚本：script/安装多语言编译运行环境.sh 
安装多语言编译运行环境.sh
```


#### 使用说明

1.  启动服务
```shell
python app.py
```

2.  浏览器打开 http://127.0.0.1:5000 即可

![Web页面](screenshot/web.png)
