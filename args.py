import platform
import re
from pathlib import Path

SYSTEM = platform.system()


def build_c_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.c')
    compile_filename = Path(code_path, name)
    compile_args = ['gcc', '-o', compile_filename.__str__(), source_filename.__str__()]
    run_args = [compile_filename.__str__()]
    return compile_args, run_args, compile_filename, source_filename


def build_cpp_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.cpp')
    compile_filename = Path(code_path, name)
    compile_args = ['g++', '-o', compile_filename.__str__(), source_filename.__str__()]
    run_args = [compile_filename.__str__()]
    return compile_args, run_args, compile_filename, source_filename


def build_java_args(code_path, *args, **kwargs):
    code = args[0] if len(args) == 1 else kwargs.get('code', '')
    try:
        class_name = re.findall(r'public\s+class\s+([a-zA-Z]+)', code)[0]
    except IndexError:
        raise Exception('必须包含一个 public 修饰的类')
    source_filename = Path(code_path, class_name + '.java')
    compile_filename = Path(code_path, class_name + '.class')
    compile_args = ['javac', '-encoding', 'UTF-8', source_filename.__str__()]
    run_args = ['java', '-classpath', code_path.__str__(), class_name]
    return compile_args, run_args, compile_filename, source_filename


def build_csharp_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.cs')
    compile_filename = Path(code_path, name + '.exe')
    if SYSTEM == 'Windows':
        compiler = 'csc'
        run_args = []
    else:
        # Linux 系统需安装 mono-devel
        compiler = 'mcs'
        run_args = ['mono']
    compile_args = [compiler, f'/out:{compile_filename.__str__()}', source_filename.__str__()]
    run_args.append(compile_filename.__str__())
    return compile_args, run_args, compile_filename, source_filename


def build_vbscript_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.vb')
    compile_filename = Path(code_path, name + '.exe')
    if SYSTEM == 'Windows':
        compiler = 'vbc'
        run_args = []
    else:
        # Linux 系统需安装 mono-devel 和 mono-vbnc
        compiler = 'vbnc'
        run_args = ['mono']
    compile_args = [compiler, f'/out:{compile_filename.__str__()}', source_filename.__str__()]
    run_args.append(compile_filename.__str__())
    return compile_args, run_args, compile_filename, source_filename


def build_php_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.php')
    compile_filename = None
    compile_args = None
    run_args = ['php', source_filename.__str__()]
    return compile_args, run_args, compile_filename, source_filename


def build_python_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.py')
    compile_filename = None
    compile_args = None
    run_args = ['python', source_filename.__str__()]
    return compile_args, run_args, compile_filename, source_filename


def build_ruby_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.rb')
    compile_filename = None
    compile_args = None
    run_args = ['ruby', source_filename.__str__()]
    return compile_args, run_args, compile_filename, source_filename


def build_golang_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.go')
    compile_filename = Path(code_path, name + ('.exe' if SYSTEM == 'Windows' else ''))
    compile_args = ['go', 'build', '-o', compile_filename.__str__(), source_filename.__str__()]
    run_args = [compile_filename.__str__()]
    return compile_args, run_args, compile_filename, source_filename


def build_rust_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.rs')
    compile_filename = Path(code_path, name)
    compile_args = ['rustc', '-o', compile_filename.__str__(), source_filename.__str__()]
    run_args = [compile_filename.__str__()]
    return compile_args, run_args, compile_filename, source_filename


def build_javascript_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.js')
    compile_filename = None
    compile_args = None
    run_args = ['node', source_filename.__str__()]
    return compile_args, run_args, compile_filename, source_filename


def build_typescript_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.ts')
    compile_filename = Path(code_path, name + '.js')
    compile_args = ['tsc', '--outDir', code_path, source_filename]
    run_args = ['node', compile_filename.__str__()]
    return compile_args, run_args, compile_filename, source_filename


def build_lua_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.lua')
    compile_filename = None
    compile_args = None
    run_args = ['lua', source_filename.__str__()]
    return compile_args, run_args, compile_filename, source_filename


def build_kotlin_args(code_path, *args, **kwargs):
    name = 'main'
    source_filename = Path(code_path, name + '.kt')
    compile_filename = Path(code_path, name + '.jar')
    compile_args = ['kotlinc', '-include-runtime', '-d', compile_filename, source_filename.__str__()]
    run_args = ['java', '-jar', compile_filename]
    return compile_args, run_args, compile_filename, source_filename


ARGS_BUILDER = {
    'c': build_c_args,
    'cpp': build_cpp_args,
    'java': build_java_args,
    'csharp': build_csharp_args,
    'vbscript': build_vbscript_args,
    'php': build_php_args,
    'python': build_python_args,
    'ruby': build_ruby_args,
    'golang': build_golang_args,
    'rust': build_rust_args,
    'javascript': build_javascript_args,
    'typescript': build_typescript_args,
    'lua': build_lua_args,
    'kotlin': build_kotlin_args
}
