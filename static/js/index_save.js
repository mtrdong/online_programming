$(document).ready(function() {
    let lang_suffix = {
        'c': '.c',
        'cpp': '.cpp',
        'java': '.java',
        'csharp': '.cs',
        'vbscript': '.vb',
        'php': '.php',
        'python': '.py',
        'ruby': '.rb',
        'golang': '.go',
        'rust': '.rs',
        'javascript': '.js',
        'typescript': '.ts',
        'lua': '.lua',
        'kotlin': '.kt'
    }
    let $select_lang = $('#select_lang')
    let $input_file = $('#input_file')
    let $button_open = $('#button_open')
    let $button_save = $('#button_save')

    $button_open.click(function() {
        $input_file.click()
    })

    $button_save.click(function() {
        let lang = $select_lang.val()
        let code = editor.getValue()
        let suffix = lang_suffix[lang]
        save_code(code, 'code' + suffix)
    })

    $input_file.change(function(e) {
        let file = e.target.files[0]
        let reader = new FileReader()
        reader.onload = function(e) {
            let content = e.target.result
            editor.setValue(content, 1)
        }
        reader.readAsText(file)
        $input_file.val(null)
    })

    function save_code(code, name) {
        let blob = new Blob([code], {type: "text/plain"})
        let url = window.URL.createObjectURL(blob)
        let link = document.createElement("a")
        link.download = name
        link.href = url
        link.style.display = "none"
        document.body.appendChild(link)
        link.click()
    }

    window.lang_suffix = lang_suffix
})
