$(document).ready(function() {
    let $select_lang = $('#select_lang')
    let $stdin = $('#stdin')
    let $input_file = $('#input_file')
    let prev_lang = $select_lang.val()
    let lang_array = ['c', 'cpp']

    $(function() {
         $('#select_lang').trigger('change')
    })

    $select_lang.change(function() {
        let lang = $select_lang.val()
        let mode = lang_array.includes(lang) ? 'c_cpp' : lang
        editor.session.setMode('ace/mode/' + mode)
        editor.session.getUndoManager().reset()
        editor.focus()
        if (['c', 'cpp', 'python'].includes(lang)) {
            $stdin.removeClass('hidden')
        } else {
            $stdin.addClass('hidden')
        }
        $stdin.val('')
        $('.right #output').html('')
        get_code_demo(lang)
        $input_file.attr('accept', window.lang_suffix[lang])
    })

    $stdin.mouseover(function() {
        let content = $stdin.val().replace(/\\n/g, "\n")
        if ($stdin[0].scrollWidth > $stdin.innerWidth()) {
            $stdin.attr('title', content)
        } else if (content.length > 0) {
            $stdin.attr('title', '标准输入(stdin)')
        } else {
            $stdin.attr('title', '无标准输入')
        }
    })

    $stdin.click(function () {
        $.confirm({
            title: '标准输入(stdin)：一行一个',
            theme: 'dark',
            content: `<textarea id="stdin_textarea" placeholder="请输入内容">${$stdin.val().replace(/\\n/g, "\n")}</textarea>`,
            boxWidth: '20%',
            useBootstrap: false,
            animation: 'none',
            buttons: {
                confirm: {
                    text: '确认',
                    action: function () {
                        let stdin = $('#stdin_textarea').val().replace(/\n/g, "\\n")
                        $stdin.val(stdin)
                    }
                },
                cancel: {
                    text: '取消',
                    action: function () {
                    }
                }
            }
        })
    })

    function get_code_demo(lang) {
        let data = {
            lang: lang,
            prev_lang: prev_lang,
            prev_code: editor.getValue()
        }

        $.post('/get_code_demo', data, function(json) {
            window.toolbar_enabled()
            if (json.code) {
                editor.setValue(json.code, 1)
            }
            prev_lang = lang
        })
    }
})