$(document).ready(function() {
    let $left_div = $('div#wrapper div.left')
    let $middle_div = $('div#wrapper div.middle')
    let $right_div = $('div#wrapper div.right')

    let document_width = $(window).width() - 0.4
    let left_div_width_percent = 0.5
    let left_div_width = document_width * left_div_width_percent

    let mouse_has_down = false
    let init_clientX = 0
    let init_left_width = 0
    let clientX_drift = 0

    $middle_div.mousedown(function(event) {
        mouse_has_down = true
        init_clientX = event.clientX
        init_left_width = $left_div.width()
        $right_div.css({
            'user-select': 'none',
            '-moz-user-select': 'none'
        })
        $('body').css({
            'cursor': 'e-resize'
        })
    })

    $(document).mouseup(function() {
        mouse_has_down = false
        $right_div.css({
            'user-select': '',
            '-moz-user-select': ''
        })
        $('body').css({
            'cursor': 'default'
        })
    })

    $(document).mousemove(function(event) {
        if (mouse_has_down) {
            clientX_drift = init_clientX - event.clientX;
            if (init_left_width - clientX_drift > 100 && document_width - (init_left_width - clientX_drift - 8) > 100) {
                left_div_width = init_left_width - clientX_drift
                left_div_width_percent = left_div_width / document_width
                $left_div.width(left_div_width)
                reset_right_div_width()
            }
        }
    })

    $(window).resize(function() {
        document_width = $(window).width() - 0.4
        left_div_width = Math.ceil(document_width * left_div_width_percent)
        $left_div.width(left_div_width)
        reset_right_div_width()
        reset_three_div_height()
    })

    function reset_right_div_width() {
        let rdw = document_width - left_div_width - $middle_div.width()
        $right_div.width(rdw)
    }

    function reset_three_div_height() {
        let all_height = document.documentElement.clientHeight - $(".toolbar").height()
        $left_div.height(all_height)
        $middle_div.height(all_height)
        $right_div.height(all_height)
    }
})
