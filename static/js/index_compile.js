$(document).ready(function() {

    let $toolbar = $('.toolbar')
    let $button_run = $('#button_run')
    let $output = $('#output')
    let timer = null

    $button_run.click(function() {
        let code = editor.getValue()
        let lang = $toolbar.find('select#select_lang').val()
        let stdin = $('#stdin').val()
        post_code(code, lang, stdin)
    })

    function post_code(code, lang, stdin) {
        toolbar_disabled()

        let lang_name = $('select#select_lang option:selected').text()
        $output.html(lang_name + '编译中')
        let timer_count = 0
        timer = setInterval(function() {
            timer_count++
            $output.html(lang_name + '编译中' + '.'.repeat(timer_count))
            if (timer_count >= 6) {
                timer_count = 0
            }
        }, 100)

        $.post('/compile', {
            code: code,
            lang: lang,
            stdin: stdin
        }, function(json) {
            toolbar_enabled()
            editor.focus()
            if (json.success) {
                $output.html(json.message)
            } else {
                $output.html(json.message)
            }
        })
    }

    function toolbar_disabled() {
        $button_run.prop('disabled', true)
        $button_run.find('polygon').css({
            fill: '#767676'
        })
    }

    function toolbar_enabled() {
        clearInterval(timer)
        $button_run.prop('disabled', false)
        $button_run.find('polygon').css({
            fill: '#3CB371'
        })
    }

    window.toolbar_disabled = toolbar_disabled
    window.toolbar_enabled = toolbar_enabled
})
